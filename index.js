const express = require("express");
const app = express();
const port = process.env.port || 3000;

const pdfjsLib = require("pdfjs-dist/legacy/build/pdf.js");
const Papa = require("papaparse");
const fs = require('fs');
let data = [];

app.use(express.json());

// app.get("/readfiles", (req, res) => {
//   res.send("GET is called");
//   pdfToCsv();
// });

app.post("/pdftocsv", async (req, res) => {
  res.send("PDF TO CSV");
  let fileDetails = req.body.fileDetails;
  let numberofFiles = fileDetails.length;
  console.log(fileDetails)
  console.log(numberofFiles)
  for (let i = 0; i < numberofFiles; i++) {
    let filename = fileDetails[i].name;
    console.log(filename)
    await getPdfDocumentText(filename);
  }
  let csv = Papa.unparse({
    fields: ["name", "text"],
    data: data,
  });
  console.log(csv);
  fs.writeFileSync('output.csv', "", {encoding:'utf8',flag:'w'});
  fs.writeFileSync('output.csv', csv, {encoding:'utf8',flag:'w'});
});

app.listen(port, (req, res) => {
  console.log(`Server listening on ${port}`);
});

// async function getDocumentText(filename) {
//   let fileDetails = [filename];
//   const urlPath = "./pdfFiles/" + filename;
//   const loadingTask = pdfjsLib.getDocument({ url: urlPath });
//   const pdfDocument = await loadingTask.promise;
//   let docString = "";
//   for (let i = 1; i <= pdfDocument.numPages; i++) {
//     const page = await pdfDocument.getPage(i);
//     const textContent = await page.getTextContent();
//     const textRows = textContent.items;
//     textRows.forEach((e) => {
//       docString = docString + e.str;
//     });
//   }
//   fileDetails.push(docString);
//   data.push(fileDetails);
// }

// async function pdfToCsv() {
//   for (let i = 0; i < 4; i++) {
//     var filename = "demo" + i + ".pdf";
//     await getDocumentText(filename);
//   }
//   let csv = Papa.unparse({
//     fields: ["name", "text"],
//     data: data,
//   });
//   console.log(csv);
//   fs.writeFileSync('output.csv', csv);
// }

async function getPdfDocumentText(filename) {
  let fileDetails = [filename];
  const urlPath = "/home/joann/Desktop/USAcute/medcat_tool/shared_folder/pdf_files/" + filename;
  const loadingTask = pdfjsLib.getDocument({ url: urlPath });
  const pdfDocument = await loadingTask.promise;
  let docString = "";
  for (let i = 1; i <= pdfDocument.numPages; i++) {
    const page = await pdfDocument.getPage(i);
    const textContent = await page.getTextContent();
    const textRows = textContent.items;
    textRows.forEach((e) => {
      docString = docString + e.str;
    });
  }
  fileDetails.push(docString);
  data.push(fileDetails);
}